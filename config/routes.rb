Rails.application.routes.draw do
  root 'static_pages#home'
  get 'static_pages/home'
  get 'static_pages/help'

  devise_for :users, path: 'devise_users'

  resources :divisions
  resources :users
end
