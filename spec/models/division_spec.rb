require 'rails_helper'

RSpec.describe Division, type: :model do
  context 'associations' do
    it { is_expected.to have_many :users }
  end

  context 'validates' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
  end

  context 'columns' do
    it { should have_db_column(:name).of_type(:string) }
    it { should have_db_column(:description).of_type(:string) }
    it { should have_db_column(:users_count).of_type(:integer) }
  end

  context 'scope' do
    let!(:division1) { FactoryGirl.create :division }
    let!(:division2) { FactoryGirl.create :division }
    let!(:division3) { FactoryGirl.create :division }
    it 'list all division expect current division' do
      divisions = Division.all_except(division1)
      expect(divisions).to eq [division2, division3]
    end
  end
end
