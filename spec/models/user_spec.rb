require 'rails_helper'

RSpec.describe User, type: :model do
  context 'associations' do
    it { is_expected.to belong_to :division }
  end

  context 'validates' do
    let!(:division) { FactoryGirl.create :division }
    let!(:birthday) { Time.zone.now - Settings.age_working.year }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }

    it 'contract date is valid' do
      expect(FactoryGirl.build(:user,
                               contract_date: (Time.zone.now - 1.day),
                               division_id: division.id)).to be_valid
    end

    it 'contract date is invalid' do
      expect(FactoryGirl.build(:user,
                               contract_date: (Time.zone.now + 1.day),
                               division_id: division.id)).not_to be_valid
    end

    it 'birthday is valid' do
      expect(FactoryGirl.build(:user,
                               birthday: birthday,
                               division_id: division.id)).to be_valid
    end

    it 'birthday is invalid' do
      expect(FactoryGirl.build(:user,
                               birthday: Time.zone.now,
                               division_id: division.id)).not_to be_valid
    end

    it 'employee have owned manager is invalid' do
      expect(FactoryGirl.build(:user,
                               role: 2,
                               division_id: division.id,
                               managers_count: 1)).not_to be_valid
    end

    it 'employee have owned manager is valid' do
      expect(FactoryGirl.build(:user,
                               role: 1,
                               division_id: division.id,
                               managers_count: 1)).to be_valid
    end
  end

  context 'columns' do
    it { should have_db_column(:name).of_type(:string) }
    it { should have_db_column(:email).of_type(:string) }
    it { should have_db_column(:avatar).of_type(:string) }
    it { should have_db_column(:role).of_type(:integer) }
    it { should have_db_column(:division_id).of_type(:integer) }
    it { should have_db_column(:manager_id).of_type(:integer) }
    it { should have_db_column(:gender).of_type(:integer) }
    it { should have_db_column(:birthday).of_type(:datetime) }
    it { should have_db_column(:contract_date).of_type(:datetime) }
  end
end
