5.times do |u|
  Division.create! name: 'Division' + (u + 1).to_s,
                   description: 'Where the dream begin'
end

User.create! name: 'Admin',
             email: 'admin@gmail.com',
             password: '123456',
             password_confirmation: '123456',
             role: 0,
             division_id: 1,
             gender: 1

5.times do |u|
  User.create! name: 'Manager' + (u + 1).to_s,
               email: "manager#{u + 1}@gmail.com",
               password: '123456',
               password_confirmation: '123456',
               role: 1,
               gender: 0,
               division_id: (u + 1).to_s,
               manager_id: 1
end

10.times do |u|
  User.create! name: 'User' + (u + 1).to_s,
               email: "user#{u + 1}@gmail.com",
               password: '123456',
               password_confirmation: '123456',
               division_id: 1,
               manager_id: 2
end

10.times do |u|
  User.create! name: 'User-' + (u + 1).to_s,
               email: "user-#{u + 1}@gmail.com",
               password: '123456',
               password_confirmation: '123456',
               division_id: 2
end

Division.all.each do |division|
  Division.update_counters division.id, users_count: division.users.length
end

User.all.each do |user|
  User.update_counters user.id, managers_count: user.managers.length
end
