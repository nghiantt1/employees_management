#
class AddUsersCountToDivisions < ActiveRecord::Migration
  add_column :divisions, :users_count, :integer, null: false, default: 0
end
