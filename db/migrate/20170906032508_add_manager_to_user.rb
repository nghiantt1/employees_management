#
class AddManagerToUser < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      t.integer :manager_id
    end

    add_index :users, :manager_id
    add_index :users, :division_id
  end
end
