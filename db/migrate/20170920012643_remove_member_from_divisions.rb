#
class RemoveMemberFromDivisions < ActiveRecord::Migration[5.0]
  def change
    remove_column :divisions, :member, :integer
  end
end
