#
class AddColumnToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :gender, :integer, default: 0
    add_column :users, :role, :integer, default: 2
    add_column :users, :birthday, :datetime
    add_column :users, :contract_date, :datetime
  end
end
