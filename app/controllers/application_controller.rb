#
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(_resource)
    if current_user.sign_in_count == 1
      edit_user_registration_path
    else
      root_path
    end
  end

  rescue_from CanCan::AccessDenied do
    flash[:alert] = t 'you_do_not_have_access'
    redirect_back fallback_location: :back
  end

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit :sign_up, keys:
      %i[name email password password_confirmation remember_me avatar]
    devise_parameter_sanitizer.permit :account_update, keys:
      %i[name email password password_confirmation current_password avatar]
  end
end
