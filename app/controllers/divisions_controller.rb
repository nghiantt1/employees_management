#
class DivisionsController < ApplicationController
  load_and_authorize_resource
  before_action :check_exist_users, only: :destroy
  before_action :load_all_division, only: :index

  def index
    respond_to do |format|
      format.csv { render text: @divisions.to_csv }
      format.xls { render text: @divisions.to_csv(col_sep: "\t") }
      format.html { render partial: 'more_action' } if request.xhr?
      format.html
    end
  end

  def show
    @employees_in_div = @division.users.order(updated_at: :desc)
                                 .page(params[:page])
                                 .per Settings.division_per_page
    @divisions = Division.all_except(@division.id)
  end

  def new
    @division = Division.new
  end

  def create
    @division = Division.new division_params
    if @division.save
      flash[:success] = t '.save'
      redirect_to divisions_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @division.update_attributes division_params
      redirect_to divisions_path
      flash[:success] = t '.updated'
    else
      render :edit
    end
  end

  def destroy
    return unless @division.destroy
    flash[:success] = t '.deleted'
  end

  private

  def division_params
    params.require(:division).permit Division::ATTR_PARAMS
  end

  def check_exist_users
    return unless @division.users.any?
    flash[:danger] = t '.can_not_delete'
    redirect_to divisions_path
  end

  def load_all_division
    @divisions = Division.all.order(created_at: :desc).page(params[:page])
                         .per Settings.division_per_page
  end
end
