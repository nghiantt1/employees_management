#
class UsersController < ApplicationController
  load_and_authorize_resource
  before_action :load_divison, except: %i[index show destroy]
  before_action :check_role_user, only: :destroy
  before_action :load_managers, only: %i[show edit]
  before_action :load_user, only: :update
  before_action :load_all_user, only: :index

  def index
    respond_to do |format|
      format.csv { render text: @users.to_csv }
      format.xls { render text: @users.to_csv(col_sep: "\t") }
      format.html { render partial: 'more_action' } if request.xhr?
      format.html
    end
  end

  def show
    @user_have_no_managers = User.user_have_no_manager
    respond_to do |format|
      if request.xhr?
        format.html do
          render partial: 'add_owned_user',
                 locals: { user_have_no_managers: @user_have_no_managers }
        end
      end
      format.html
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new create_user_params
    if @user.save
      # SendEmailWorkerAccount.perform_async @user.id
      AccountMailer.user_account(@user).deliver_now
      flash[:success] = t '.save'
      redirect_to users_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if current_user.admin?
      exec_with_admin
    else
      update_user_params user_edit_params
    end
  end

  def destroy
    return unless @user.destroy
    flash[:success] = t '.deleted'
  end

  private

  def exec_with_admin
    if request.xhr?
      exec_request_xhr
    elsif params[:reset].present?
      reset_password_when_edit_user
    else
      update_user_params user_params
    end
  end

  def exec_request_xhr
    if params[:reset_password].present?
      multi_reset_password
    elsif params[:manager_id].present?
      @user.update_attributes manager_id: params[:manager_id]
    else
      @user.update_attributes division_id: params[:division_id]
    end
  end

  def multi_reset_password
    @user.update_attributes password: User.random_password
    PasswordMailer.user_password(@user).deliver_now
  end

  def reset_password_when_edit_user
    update_user_params(user_params.merge!(password: User.random_password))
    PasswordMailer.user_password(@user).deliver_now
  end

  def update_user_params(params)
    if @user.update_attributes params
      flash[:success] = t '.updated'
      redirect_to user_path
    else
      render :edit
    end
  end

  def create_user_params
    user_params.merge! password: User.random_password
  end

  def user_params
    params.require(:user).permit :name, :email, :gender, :birthday, :avatar,
                                 :role, :contract_date, :division_id
  end

  def user_edit_params
    params.require(:user).permit :name, :email, :gender, :birthday, :avatar
  end

  def load_divison
    @divisions = Division.all
  end

  def check_role_user
    return unless @user.admin? || @user.manager?
    flash[:danger] = t '.can_not_delete'
    redirect_to users_path
  end

  def load_managers
    @managers = @user.managers.order(updated_at: :desc).page(params[:page])
                     .per Settings.division_per_page
  end

  def load_user
    @user = User.find_by id: params[:id]
    return if @user.present?
    flash.now[:danger] = t '.not_found_user'
  end

  def load_all_user
    @users = User.all.order(updated_at: :desc).includes(:division)
                 .page(params[:page]).per Settings.user_per_page
  end
end
