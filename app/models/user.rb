#
class User < ApplicationRecord
  include ApplicationHelper

  belongs_to :division, counter_cache: true
  belongs_to :manager, class_name: User.name, foreign_key: :manager_id,
                       optional: true
  has_many :managers, class_name: User.name, foreign_key: :manager_id, 
                      counter_cache: true

  mount_uploader :avatar, AvatarUploader
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true
  validates :email, presence: true
  validate :validate_contract_date
  validate :validate_birthday
  validate :validate_update_role_for_admin_or_manager

  delegate :name, to: :division, prefix: true
  delegate :name, to: :manager, prefix: true

  enum role: %w[admin manager employee]
  enum gender: %w[male female other]

  scope :user_have_no_manager, lambda {
    where('manager_id IS NULL AND role = ?', User.roles[:employee])
  }

  class << self
    def random_password
      chars = (0..9).to_a + ('a'..'z').to_a + ('A'..'Z').to_a
      chars.sample(8).join
    end

    def to_csv(options = {})
      config_columns = %w[id name email role division_id]
      CSV.generate(options) do |csv|
        csv << config_columns
        all.each do |user|
          csv << user.attributes.values_at(*config_columns)
        end
      end
    end
  end

  private

  def validate_contract_date
    return unless contract_date.present?
    now = format_time Time.zone.now, :format_date
    date = format_time contract_date, :format_date
    return errors.add :error, I18n.t('.invalid_contract_date') if date > now
  end

  def validate_birthday
    return unless birthday.present?
    user_age = Time.zone.now.year - birthday.year
    return unless user_age < Settings.age_working
    errors.add :error, I18n.t('.invalid_birthday')
  end

  def validate_update_role_for_admin_or_manager
    return unless employee? && (managers_count > 0)
    errors.add :error, I18n.t('.invalid_role')
  end
end
