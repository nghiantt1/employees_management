#
class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, :all
    elsif user.manager?
      can :read, :all
      can :update, Division, id: user.division_id
      can :update, User, id: user.id
    else
      can :read, :all
      can :update, User, id: user.id
    end
  end
end
