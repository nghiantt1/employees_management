#
class Division < ApplicationRecord
  has_many :users

  validates :name, presence: true
  validates :description, presence: true

  ATTR_PARAMS = %i[name description].freeze

  scope :all_except, ->(id) { where.not id: id }

  def self.to_csv(options = {})
    config_columns = %w[id name description]
    CSV.generate(options) do |csv|
      csv << config_columns
      all.each do |division|
        csv << division.attributes.values_at(*config_columns)
      end
    end
  end
end
