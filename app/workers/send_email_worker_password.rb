#
class SendEmailWorkerPassword
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find user_id
    PasswordMailer.user_password(user).deliver_now
  end
end
