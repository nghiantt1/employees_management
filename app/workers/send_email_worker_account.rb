#
class SendEmailWorkerAccount
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find user_id
    AccountMailer.user_account(user).deliver_now
  end
end
