$(document).ready(function(){
  $('.tooltipped').tooltip({delay: 50});

  $('.parallax').parallax();

  $('.modal').modal();

  $('.datepicker').pickadate({
    selectMonths: true,
    selectYears: 15,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false
  });

  $('.slider').slider();

  $(function() {
    $(window).scroll(function() {
      if ($(this).scrollTop() > 100) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }

      // if ($(this).scrollTop() > 300) {
      //   $('#detail-division').addClass('fixed-detail');
      // } else {
      //   $('#detail-division').removeClass('fixed-detail');
      // }
   });
    $('#back-to-top').click(function() {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
    });
  });

  $('select').material_select();

  $(document).on('click', '.delete-division', function(){
    var id = $(this).attr('value');
    var member_count = $('#member' + id).attr('value');
    if(member_count != 0){
      swal(I18n.t("divisions.index.alert"),
        I18n.t("divisions.index.text_under"));
    } else {
      swal({
        title: I18n.t("divisions.index.confirm"),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: I18n.t("divisions.index.accept")
      }).then(function() {
        $.ajax({
          url: '/divisions/' + id,
          method: 'delete',
          success: function(){
            $('#division' + id).remove();
            swal(I18n.t("divisions.index.deleted"),
              I18n.t("divisions.index.delete_success"), "success");
          },
          error: function() {
            swal(
              I18n.t('action.oops'),
              I18n.t('action.sth_wrong'),
              'error'
            );
          }
        });
      }).done();
    }
  })

  $(document).on('click', '.delete-employee', function(){
    var id = $(this).attr('value');
    var role = $('.user-role' + id).attr('value');
    if(role === "admin" || role === "manager") {
      swal(I18n.t("users.index.alert"),
        I18n.t("users.index.text_under"));
    } else {
      swal({
        title: I18n.t("divisions.index.confirm"),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: I18n.t("divisions.index.accept")
      }).then(function() {
        $.ajax({
          url: '/users/' + id,
          method: 'delete',
          success: function(){
            $('#user' + id).remove();
            swal(I18n.t("divisions.index.deleted"),
              I18n.t("users.index.delete_success"), "success");
          },
          error: function() {
            swal(
              I18n.t('action.oops'),
              I18n.t('action.sth_wrong'),
              'error'
            );
          }
        });
      }).done();
    }
  })

  $('#reset-password').on('click', function(){
    var checkbox = document.getElementById('reset-password');
    if (checkbox.checked === true) {
      $('#reset-answer').empty();
      $('#reset-answer').append('Yes');
    } else {
      $('#reset-answer').empty();
      $('#reset-answer').append('No');
    }
  });

  var checkboxes = $('.card').find('#content-color tbody input[type = "checkbox"]');
  var reset_all = document.getElementById('reset-password-all');

  $(document).on('click', '#reset-password-all', function() {
    if (reset_all.checked === true) {
      for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = true;
      }
    } else {
      for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = false;
      }
    }
  });

  $(document).on('click', '.reset-password', function(){
    enableAction();

    showCheckboxButton();

    $('.btn-action-allowed').on('click', function(){
      swal({
        title: I18n.t("users.index.confirm"),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: I18n.t("users.index.accept")
      }).then(function() {
        resetPassword();
        actionDeny(reset_all, checkboxes);
      }).done();
    });

    disabledAction();
  });

  function resetPassword() {
    for (i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked === true) {
        var id = parseInt(checkboxes[i].value);
        $.ajax({
          url: '/users/' + id,
          method: 'PATCH',
          data: {reset_password: true},
          success: function(){
            swal(I18n.t("users.index.reseted"),
              I18n.t("users.index.reset_success"), "success");
            $('#fixed-button').addClass('hidden-fixed-button');
          },
          error: function() {
            swal(
              I18n.t('action.oops'),
              I18n.t('action.sth_wrong'),
              'error'
            );
          }
        });
      }
    }
  }

  $(document).on('click', '.delete-employees', function(){
    enableAction();

    showCheckboxButton();

    $('.btn-action-allowed').on('click', function(){
      swal({
        title: I18n.t("users.index.confirm_delete"),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: I18n.t("users.index.accept_delete")
      }).then(function() {
        deleteEmployee();
        actionDeny(reset_all, checkboxes);
      }).done();
    });

    disabledAction();
  });

  function deleteEmployee() {
    for (i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked === true) {
        var id = parseInt(checkboxes[i].value);
        $.ajax({
          url: '/users/' + id,
          method: 'DELETE',
          success: function(){
            $('#user' + id).remove();
            swal(I18n.t("divisions.index.deleted"),
              I18n.t("users.index.delete_success"), "success");
          },
          error: function() {
            swal(
              I18n.t('action.oops'),
              I18n.t('action.sth_wrong'),
              'error'
            );
          }
        });
      }
    }
  }

  $(document).on('click', '#move-all', function() {
    if (move_all.checked === true) {
      for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = true;
      }
    } else {
      for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = false;
      }
    }
  });

  function enableAction() {
    reset_all.disabled = false;
    for (i = 0; i < checkboxes.length; i++) {
      checkboxes[i].disabled = false;
    }
  }

  function showCheckboxButton() {
    $('#fixed-button').removeClass('hidden-fixed-button');
  }

  function disabledAction() {
    $('.btn-action-deny').on('click', function(){
      actionDeny(reset_all, checkboxes);
      $('#fixed-button').addClass('hidden-fixed-button');
    });
  }

  function actionDeny(all, checkbox) {
    all.disabled = true;
    all.checked = false;
    for (i = 0; i < checkbox.length; i++) {
      checkbox[i].disabled = true;
      checkbox[i].checked = false;
    }
  }

  var move_all = document.getElementById('move-all');

  $(document).on('click', '.move-employees', function(){
    enableActionMove();

    showCheckboxButton();

    $('.btn-action-allowed').on('click', function(){
      $('#modal-move-to-division').modal('open');
    });

    disabledActionMove();
  });

  $('.btn-allow-move').on('click', function(){
    swal({
      title: I18n.t("divisions.show.confirm"),
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: I18n.t("divisions.show.move")
    }).then(function() {
      moveEmployees();
      actionDeny(move_all, checkboxes);
      $('#modal-move-to-division').modal('close');
    }).done();
  })

  function moveEmployees() {
    var division_choosen = parseInt($('#modal-move-to-division')
      .find('input[type = "radio"]:checked').attr('value'));
    for (i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked === true) {
        var id = parseInt(checkboxes[i].value);
        $.ajax({
          url: '/users/' + id,
          method: 'PATCH',
          data: {division_id: division_choosen},
          success: function(){
            $('#employee' + id).remove();
            swal(I18n.t("divisions.show.moved"),
              I18n.t("divisions.show.move_success"), "success");
            $('#fixed-button').addClass('hidden-fixed-button');
          },
          error: function() {
            swal(
              I18n.t('action.oops'),
              I18n.t('action.sth_wrong'),
              'error'
            );
          }
        });
      }
    }
  }

  function enableActionMove() {
    move_all.disabled = false;
    for (i = 0; i < checkboxes.length; i++) {
      checkboxes[i].disabled = false;
    }
  }

  function disabledActionMove() {
     $('.btn-action-deny').on('click', function(){
      actionDeny(move_all, checkboxes);
      $('#fixed-button').addClass('hidden-fixed-button');
    });
  }

  $('.add-owned-employee').on('click', function(){
    var number_user = $('#number-user').val();
    if (number_user > 0) {
      var id = $('#manager-id').val();
      $.ajax({
        dataType: 'html',
        url: '/users/' + id,
        method: 'GET',
        success: function(data) {
          $('.show-user-have-no-manager').html(data);
        },
        error: function() {
          swal(
            I18n.t('action.oops'),
            I18n.t('action.sth_wrong'),
            'error'
          );
        }
      });
    } else {
      swal(
        I18n.t('action.oops'),
        I18n.t('action.no_user'),
        'info'
      );
    }
  });

  $('body').on('click', '.close-add-user', function(){
    $('.show-user-have-no-manager').empty();
  });

  $('body').on('click', '#add-user-all', function(){
    var checkbox_all = $('.card').find('#content-color tbody input[type = "checkbox"]');
    var add_all = document.getElementById('add-user-all');
    if (add_all.checked) {
      for (i = 0; i < checkbox_all.length; i++) {
        checkbox_all[i].checked = true;
      }
    } else {
      for (i = 0; i < checkbox_all.length; i++) {
        checkbox_all[i].checked = false;
      }
    }
  });

  $('body').on('click', '.ok-add-user', function(){
    var checkbox_all = $('.card').find('#content-color tbody input[type = "checkbox"]');
    swal({
      title: I18n.t("users.show.confirm"),
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: I18n.t("users.show.add")
    }).then(function() {
      addOwnedUser();
    }).done();

    function addOwnedUser() {
      var manager_choosen = $('#manager-id').val();
      for (i = 0; i < checkbox_all.length; i++) {
        if (checkbox_all[i].checked === true) {
          var id = parseInt(checkbox_all[i].value);
          $.ajax({
            url: '/users/' + id,
            method: 'PATCH',
            data: {manager_id: manager_choosen},
            success: function(){
              window.location.reload();
            },
            error: function() {
              swal(
                I18n.t('action.oops'),
                I18n.t('action.sth_wrong'),
                'error'
              );
            }
          });
        }
      }
    }
  });

  $('.more-action').on('click', function(e){
    showMoreAction('divisions');
    e.stopPropagation();
  });

  $('.more-action-user').on('click', function(e){
    showMoreAction('users');
    e.stopPropagation();
  });

  var showMoreAction = function(url){
    $.ajax({
      url: '/' + url,
      method: 'GET',
      dataType: 'html',
      success: function(data){
        $('.multi-action').html(data);
      },
      error: function() {
        swal(
          I18n.t('action.oops'),
          I18n.t('action.sth_wrong'),
          'error'
        );
      }
    });
  }

  $('body').on('click', function(){
    $('.multi-action').empty();
  });

  $('.delete-employee-in-div').on('click', function(){
    var id = $(this).attr('value');
    swal({
      title: I18n.t("divisions.index.confirm"),
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: I18n.t("divisions.index.accept")
    }).then(function() {
      $.ajax({
        url: '/users/' + id,
        method: 'DELETE',
        success: function(){
          $('#employee' + id).remove();
          swal(I18n.t("divisions.index.deleted"),
          I18n.t("users.index.delete_success"), "success");
        },
        error: function(){
          swal(
            I18n.t('action.oops'),
            I18n.t('action.sth_wrong'),
            'error'
          );
        }
      });
    }).done();
  });
});
