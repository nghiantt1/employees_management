#
class PasswordMailer < ApplicationMailer
  def user_password(user)
    @user = user
    mail to: user.email, subject: t('.reset_password')
  end
end
