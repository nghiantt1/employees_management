#
class AccountMailer < ApplicationMailer
  def user_account(user)
    @user = user
    mail to: user.email, subject: t('.creat_account')
  end
end
